#!/usr/bin/python3

import pymysql

# Open database connection
db = pymysql.connect("localhost","root" )

cur = db.cursor()

cur.execute("CREATE DATABASE IF NOT EXISTS database_liquibase")

cur.execute("SHOW DATABASES")
cur.execute("SHOW GLOBAL VARIABLES LIKE 'PORT'")

# print all the first cell of all the rows
for row in cur.fetchall():
    print(row[0])

db.close()