echo "Install liquibase"
ls /usr/local
cat /etc/os-release
pwd
cd /
pwd
apt-get install tree


cd /usr/local
mkdir liquibase
cd /usr/local/liquibase


wget https://github.com/liquibase/liquibase/releases/download/v3.10.3/liquibase-3.10.3.tar.gz

FILE=liquibase-3.10.3.tar.gz
if test -f "$FILE"; then
    echo "$FILE exists."
fi

tar xvzf liquibase-3.10.3.tar.gz
rm liquibase-3.10.3.tar.gz

export PATH=$PATH:/usr/local/liquibase

cat /etc/os-release

echo "$PATH"

echo "Install openjdk"
cd /usr/local/
mkdir -p java
cd java
export PATH=$PATH:/usr/local/java/jdk-11/bin
echo "$PATH"
curl -O https://download.java.net/openjdk/jdk11/ri/openjdk-11+28_linux-x64_bin.tar.gz
FILE=openjdk-11+28_linux-x64_bin.tar.gz
if test -f "$FILE"; then
    echo "$FILE exists.!!!!!!"
fi
tar xvzf openjdk-11+28_linux-x64_bin.tar.gz
rm openjdk-11+28_linux-x64_bin.tar.gz
cd /
echo "check java"
java -version

liquibase --help
echo "Install ......................Oks"

echo "Install ......................mysql_connector"
find . -name "*.jar"
cd /usr/local/
mkdir mysql_connector
cd mysql_connector
wget https://downloads.mysql.com/archives/get/p/3/file/mysql-connector-java_8.0.22-1debian9_all.deb
tar xvzf mysql-connector-java_8.0.22-1debian9_all.deb
FILE=mysql-connector-java_8.0.22-1debian9_all.deb
if test -f "$FILE"; then
    echo "$FILE exists.!!!!!!"
fi
dpkg -i mysql-connector-java_8.0.22-1debian9_all.deb
tree

echo "Install ......................mysql_connector=>>>> Okc"
pwd
cd /
pwd
find . -name "*.jar"

export CLASSPATH=$CLASSPATH:/usr/share/java/mysql-connector-java-8.0.22.jar

mysql -V

liquibase \
    --logLevel=info \
    --classpath=/usr/share/java/mysql-connector-java-8.0.22.jar \
    --driver=com.mysql.jdbc.Driver \
    --url=jdbc:mysql://localhost:3306/pipelines \
    --username=root \
    --password=root \
    --changeLogFile=/usr/local/changelog.xml \
    update