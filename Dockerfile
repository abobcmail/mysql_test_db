FROM python:3.5.2
FROM liquibase/liquibase:3.10.3 AS Liquibase
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update \
  && apt-get install -y mysql-server --no-install-recommends \
  && apt-get clean \
  && pip3 install PyMySQL \ 
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY changelog/ /usr/local/





# This Dockerfile doesn't need to have an entrypoint and a command
# as Bitbucket Pipelines will overwrite it with a bash script.